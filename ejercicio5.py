"""                            author: "Angel Morocho"
                       e-mail: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 5:
            Escribe un programa que le pida al usuario una temperatura en grados Celsius,
            la convierta a grados Fahrenheit e imprima por pantalla la temperatura convertida."""


while True:
    try:
        date = input("Ingrese los grados centigrados: \n")
        celsius = float(date)
        temp = (celsius * 9 / 5) + 32
        print("Los", celsius, "grados Celsius convertidos a Farenheit son:\n", temp)
        break
    except ValueError:
        print("Incorrecto: Ingrese un numero")