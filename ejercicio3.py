"""                        author: "Angel Morocho"
                   e-mail: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 3:
             Escribe un programa para pedirle al usuario el número de horas y la
             tarifa por hora para calcular el salario bruto."""


while True:
    try:
        laboradas = int(input("ingrese el numero de horas laboradas: \n"))
        while True:
            try:
                tarifa = float(input("ingrese la tarifa por hora: \n "))
                # procesos
                salario = (laboradas * tarifa)
                # mensaje/resultado
                print("Bienvenido, su salario es de:", salario)
                break
            except ValueError:
                print("Tarifa incorrecta: Ingrese un numeros")
        break
    except ValueError:
        print("Hora incorrecta: Ingrese un numeros")
