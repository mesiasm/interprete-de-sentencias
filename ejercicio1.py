"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 1:
            Escribe las siguientes sentencias en el intérprete de Python para comprobar qué hacen:"""

5
x = 5  # Asigna un valor a la variable
x + 1   # Realiza un proceso de suma pero no presenta
print(x + 1)   # Presenta un resultado
""" Este programa realiza la sumatoria de la variable pero no muestra debido a que no existe un comando para
    salida/presentacion de datos. Pero si presentaramos el dato x+1 nos presenta el resultado de dicha variable. """

